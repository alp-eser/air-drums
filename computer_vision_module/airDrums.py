import cv2 as cv
import numpy as np
import time
import simpleaudio as sa
import threading


def playSnare(soundType):
    if soundType == 'S':
        wave_obj = sa.WaveObject.from_wave_file("snare.wav")
        play_obj = wave_obj.play()
        play_obj.wait_done()
    elif soundType == 'H':
        wave_obj = sa.WaveObject.from_wave_file("hihat.wav")
        play_obj = wave_obj.play()
        play_obj.wait_done()

# Real World measurements
realWorldDistance = 85.5 # in centimeters
realWorldBallDiameter = 4 #centimeters
cam_number = 0

#Speed Parameters for RED
R_initialTime = 0
R_initialDistance = 0
R_changeInTime = 0
R_changeInDistance = 0
R_listDistance = []
R_listSpeed = []

#Speed Parameters for BLUE
B_initialTime = 0
B_initialDistance = 0
B_changeInTime = 0
B_changeInDistance = 0
B_listDistance = []
B_listSpeed = []

camera = cv.VideoCapture(cam_number)
camera.set(3,640)
camera.set(4,480)

def speedFinder(coveredDistance, timeTaken):

    speed = coveredDistance / timeTaken

    return speed

def averageFinder(completeList, averageOfItems):

    # finding the length of list.
    lengthOfList = len(completeList)

    # calculating the number items to find the average of
    selectedItems = lengthOfList - averageOfItems
    # 10 -6 =4

    # getting the list most recent items of list to find average of .
    selectedItemsList = completeList[selectedItems:]

    # finding the average .
    average = sum(selectedItemsList) / len(selectedItemsList)

    return average

def stackImages(scale,imgArray):
    rows = len(imgArray)
    cols = len(imgArray[0])
    rowsAvailable = isinstance(imgArray[0], list)
    width = imgArray[0][0].shape[1]
    height = imgArray[0][0].shape[0]
    if rowsAvailable:
        for x in range ( 0, rows):
            for y in range(0, cols):
                if imgArray[x][y].shape[:2] == imgArray[0][0].shape [:2]:
                    imgArray[x][y] = cv.resize(imgArray[x][y], (0, 0), None, scale, scale)
                else:
                    imgArray[x][y] = cv.resize(imgArray[x][y], (imgArray[0][0].shape[1], imgArray[0][0].shape[0]), None, scale, scale)
                if len(imgArray[x][y].shape) == 2: imgArray[x][y]= cv.cvtColor( imgArray[x][y], cv.COLOR_GRAY2BGR)
        imageBlank = np.zeros((height, width, 3), np.uint8)
        hor = [imageBlank]*rows
        hor_con = [imageBlank]*rows
        for x in range(0, rows):
            hor[x] = np.hstack(imgArray[x])
        ver = np.vstack(hor)
    else:
        for x in range(0, rows):
            if imgArray[x].shape[:2] == imgArray[0].shape[:2]:
                imgArray[x] = cv.resize(imgArray[x], (0, 0), None, scale, scale)
            else:
                imgArray[x] = cv.resize(imgArray[x], (imgArray[0].shape[1], imgArray[0].shape[0]), None,scale, scale)
            if len(imgArray[x].shape) == 2: imgArray[x] = cv.cvtColor(imgArray[x], cv.COLOR_GRAY2BGR)
        hor= np.hstack(imgArray)
        ver = hor
    return ver

def findRedBall(img):
    imgHSV = cv.cvtColor(img,cv.COLOR_BGR2HSV)

    # ===================== RED
    R_h_min = 0
    R_s_min = 0
    R_v_min = 16

    R_h_max = 18
    R_s_max = 255
    R_v_max = 255

    R_lower = np.array([R_h_min,R_s_min,R_v_min])
    R_upper = np.array([R_h_max,R_s_max,R_v_max])
    R_mask = cv.inRange(imgHSV,R_lower,R_upper)
    
    return R_mask

def findBlueBall(img):
    imgHSV = cv.cvtColor(img,cv.COLOR_BGR2HSV)

    #==================== Blue 
    B_h_min = 98
    B_s_min = 0
    B_v_min = 0

    B_h_max = 179
    B_s_max = 255
    B_v_max = 255

    B_lower = np.array([B_h_min,B_s_min,B_v_min])
    B_upper = np.array([B_h_max,B_s_max,B_v_max])
    B_mask = cv.inRange(imgHSV,B_lower,B_upper)
    
    return B_mask

def getContours(img):
    imgContoured = img.copy()
    contours, hierarchy = cv.findContours(img,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
    for cnt in contours:
        area = cv.contourArea(cnt)
        if area > 500:
            cv.drawContours(imgContoured,cnt,-1,(0,255,0),2)
            peri = cv.arcLength(cnt,True)
            approx = cv.approxPolyDP(cnt,0.02*peri,True)
            x, y, w, h = cv.boundingRect(approx)
            return w, imgContoured

def focalLengthFinder(realWorldDistance, realBallDiameter, ballDiameterInFrame):
    focalLength = (ballDiameterInFrame* realWorldDistance)/realBallDiameter
    return focalLength

def distanceFinder(realBallDiameter, focalLength, ballDiameterInFrame):
   distance= (realBallDiameter * focalLength) / ballDiameterInFrame 
   return distance 

def RedBallDetectionFromRefImg(image):
    ballWidth =0
    mask = findRedBall(image)
    ballWidth, contouredImg = getContours(mask) 
    print(ballWidth)
    return ballWidth, contouredImg

def BlueBallDetectionFromRefImg(image):
    ballWidth =0
    mask = findBlueBall(image)
    ballWidth, contouredImg = getContours(mask) 
    print(ballWidth)
    return ballWidth, contouredImg

def mapDrumsticksToBlackImage(imgMap,x,d,h,stickColor,speed,isThere):
    mapped_x = int(x)
    mapped_d = int(d*3)
    mapped_h = int(h*0.06)
    if stickColor == 'R':
        cv.circle(imgMap,(mapped_x,mapped_d),mapped_h,(0,0,225),2)
        cv.putText(imgMap,"RED X = "+str(mapped_x)+" RED D = "+str(mapped_d)+" RED H = "+str(mapped_h),(100,100),font,0.7,(0,0,255),2)
        cv.putText(imgMap,f"speed = {speed}",(100,375),font,0.7,(0,0,255),2)

    elif stickColor == 'B':
        cv.circle(imgMap,(mapped_x,mapped_d),mapped_h,(255,0,0),2)
        cv.putText(imgMap,"BLUE X = "+str(mapped_x)+" BLUE D = "+str(mapped_d)+" BLUE H = "+str(mapped_h),(100,50),font,0.7,(255,0,0),2)
        cv.putText(imgMap,f"speed = {speed}",(100,425),font,0.7,(255,0,0),2)

    if mapped_d >= 125 and mapped_d <= 300:
        if mapped_x >= 240 and mapped_x <= 440:
            if mapped_h >= 24:
                if speed >= 1:
                    if not isThere:
                        t1 = threading.Thread(target=playSnare,args=['S'])
                        t1.start()
                        print("SNAAAAAAAAAAAAAAAAAARRRRRRRRRRRREEEEEEEEE")
                        cv.putText(imgMap,"SNAAARE",(50,50),cv.FONT_HERSHEY_COMPLEX,1,(0,255,0),1)
                    return True
    
    if mapped_d >= 130 and mapped_d <= 270:
        if mapped_x >= 450 and mapped_x <= 590:
            if mapped_h >= 10:
                if speed >= 1:
                    if not isThere:
                        t2 = threading.Thread(target=playSnare,args=['H'])
                        t2.start()
                        print("HIIIIIGGGGGGHHHH HAAAAAAAT")
                        cv.putText(imgMap,"HIIIIGHHAT",(50,50),cv.FONT_HERSHEY_COMPLEX,1,(200,255,0),1)
                    return True
    return False
                

def renderMap(imgMap):
    cv.waitKey(1)
    cv.imshow("2D Map",imgMap)
    imgMap[:] = 0,0,0
    cv.circle(imgMap,(320,250),100,(255,255,255),cv.FILLED)
    cv.circle(imgMap,(520,200),60,(100,100,0),cv.FILLED)



redImgReference = cv.imread("rf_red.png")
blueImgReference = cv.imread("rf_blue.png")

# I arbitrarly chose to calculate the focal length according to the red ball.
redBallWidth , redBallImgDetected= RedBallDetectionFromRefImg(redImgReference)
calculatedFocalLength = focalLengthFinder(realWorldDistance, realWorldBallDiameter,redBallWidth)

# Either one can be used since the focal length is a fixed value
# blueBallWidth , blueBallImgDetected = BlueBallDetectionFromRefImg(blueImgReference)
# calculatedFocalLength = focalLengthFinder(realWorldDistance, realWorldBallDiameter,blueBallWidth)

print(calculatedFocalLength)
font = cv.FONT_HERSHEY_SIMPLEX 

img2Dmap = np.zeros((480,640,3),np.uint8)
isRedThere = False
isBlueThere = False
while True:
    ret, frame = camera.read()
    imgContoured = frame.copy()
    R_mask = findRedBall(frame)
    contours, hierarchy = cv.findContours(R_mask,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
    for cnt in contours:
        area = cv.contourArea(cnt)
        if area > 100:
            cv.drawContours(imgContoured,cnt,-1,(0,255,0),2)
            peri = cv.arcLength(cnt,True)
            approx = cv.approxPolyDP(cnt,0.02*peri,True)
            x, y, w, h = cv.boundingRect(approx)

            distance = distanceFinder(realWorldBallDiameter,calculatedFocalLength, w)
            print(distance)

            cv.putText(imgContoured, f" Distance = {distance}", (50,50),font, 0.7, (0,0,255), 2)
            cv.putText(imgContoured, f" X = {x} Y = {y}", (50,25),font, 0.7, (0,0,255), 2)
            
            #Calculating speed FOR RED
            R_listDistance.append(y)
            averageDistance = averageFinder(R_listDistance,2)
            distanceInMeters = averageDistance/100
            if R_initialDistance != 0:
                R_changeInDistance = R_initialDistance - distanceInMeters
            
            R_changeInTime = time.time() - R_initialTime

            speed = speedFinder(coveredDistance=R_changeInDistance,timeTaken=R_changeInTime)
            R_listSpeed.append(speed)

            averageSpeed = averageFinder(R_listSpeed,10)
            averageSpeed = averageSpeed * -1
            
            # if averageSpeed < 0:
                # averageSpeed = 0
          

            # MAPPING --------------------
            isRedThere = mapDrumsticksToBlackImage(img2Dmap,x,distance,y,'R',averageSpeed,isRedThere)

            

            #inital distance and time
            R_initialDistance = distanceInMeters
            R_initialTime = time.time()
            

    B_mask = findBlueBall(frame)
    contours, hierarchy = cv.findContours(B_mask,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
    for cnt in contours:
        area = cv.contourArea(cnt)
        if area > 500:
            cv.drawContours(imgContoured,cnt,-1,(0,255,0),2)
            peri = cv.arcLength(cnt,True)
            approx = cv.approxPolyDP(cnt,0.02*peri,True)
            x, y, w, h = cv.boundingRect(approx)

            distance = distanceFinder(realWorldBallDiameter,calculatedFocalLength, w)
            print(distance)

            cv.putText(imgContoured, f" X = {x} Y = {y}", (50,75),font, 0.7, (255,0,0), 2)
            cv.putText(imgContoured, f" Distance = {distance}", (50,100),font, 0.7, (255,0,0), 2)

            #Calculating speed FOR BLUE
            B_listDistance.append(y)
            averageDistance = averageFinder(B_listDistance,2)
            distanceInMeters = averageDistance/100
            if B_initialDistance != 0:
                B_changeInDistance = B_initialDistance - distanceInMeters
            
            B_changeInTime = time.time() - B_initialTime

            speed = speedFinder(coveredDistance=B_changeInDistance,timeTaken=B_changeInTime)
            B_listSpeed.append(speed)

            averageSpeed = averageFinder(B_listSpeed,10)
            averageSpeed = averageSpeed * -1

            isBlueThere =  mapDrumsticksToBlackImage(img2Dmap,x,distance,y,'B',averageSpeed,isBlueThere)

            #inital distance and time
            B_initialDistance = distanceInMeters
            B_initialTime = time.time()



    cv.imshow('frame', imgContoured)

    #RENDERING MAP
    renderMap(img2Dmap)


    if cv.waitKey(1)==ord('q'):
        break

camera.release()
cv.destroyAllWindows()
