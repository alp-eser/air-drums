# Airdrums

This project is an attempt to designing and implementing a device that can emulate playing the drums in other words: “air-drumming.” It was first inspired by similar projects on the internet however ones of simpler mechanisms such as: tracking two colored objects on a 2D grid using a computer’s webcam and simply producing the sound as a digital signal just like pressing a key on the keyboard. Later a decision was made to improve the system by several factors such as tracking the drumsticks in 3D space, adding acceleration measurement sensors, and finally implementing a sensor-fusion algorithm to get the best of out the results obtained from both the computer vision module and the embedded module, thus resulting in a more accurate air-drumming experience for the user. 

As mentioned earlier this project utilises features in computer vision such as: colour detection and tracking to locate two drumsticks in front of a camera, hooked to sensors that measure the acceleration of the drumsticks when the user hits a virtual drum in 3D space. During the research phase the decision was made to implement the entire system on a Raspberry pi 4 model B and STM32 micro controller for signal processing and filtering. By the end of the semester the project was assembled and implemented such that the user could choose one of two modes of playing: one using the computer vision module which pre-defines two virtual areas in 3D space one representing the snare drum and the other representing the hi-hat. The computer vision module detects and tracks the two-coloured drumsticks in each frame using a Raspberry pi camera in addition to approximating the speed of the hit and producing the corresponding drum piece sound upon reaching a certain position and velocity. 

The Second mode on the other hand, leaves the position tracking to the computer vision module and assigns the velocity calculations to the embedded module using the acceleration measurement sensors controlled by an STM32 micro-controller which processes the signal coming from the sensors and converts it to velocity eventually transmitting it to the raspberry pi through its serial port. 

## Installation

1. Clone this repository to your device.
2. install Python3.
3. install OpenCV-Python
4. Load STM32 Program onto your STM32F4 micro-controller.
5. Follow the wiring in the diagram attached in this repository.

To run the Computer Vision module only:
```bash
python3 computer_vision_module/airDrums.py
```
To run the Embedded module along with the computer vision module, make sure you have completed the wiring correctly and:

```bash
python3 embedded_module/airDrums_sensors.py
```

## License
This was a graduation project for an undergrad degree in Computer Science Engineering at Gebze Technical University.
